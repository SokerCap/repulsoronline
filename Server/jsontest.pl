
use JSON;
use JSON::XS;

my $json = JSON->new->allow_nonref([1]);

print "JSON created\n";

my @arr1 = (1..10);
my %harr1 = ("test"=>"aaaa");

#my $jstr = $json->encode_json(\@arr);
my $jstr = encode_json \%harr1 ;
print "json'd: $jstr\n";

my $jscalar = decode_json $jstr;
print "scalar decoded: ".$jscalar."\n";