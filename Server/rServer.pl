#!/usr/bin/perl
#Repulsor Server - run a lobby service and message relay for repulsor games
#Created by Ryan Guthrie
#socket code based off http://www.perlfect.com/articles/select.shtml

use IO::Socket;
use JSON;
use JSON::XS;
use strict;

my $l_server = '10.201.17.59';
my $l_port = '9091';

#my @slots = @(1..50);
my $json = JSON->new->allow_nonref([1]);
my %users; #main users hash, hashed by host address

#print "slots:".$slots[10]."\n";
my $openslots = 50; 

 # Create the receiving socket
## Open Server Socket
my $s = IO::Socket::INET->new(Proto => 'tcp',
                                LocalAddr => $l_server,
                                LocalPort => $l_port,
                                Listen => SOMAXCONN,
                                Reuse => 1) or die "Server Failed to Start : $@" ;
 
 print "Socket Created!\n";
 
 use IO::Select;
 my $read_set = new IO::Select(); # create handle set for reading
 $read_set->add($s);           # add the main socket to the set
 
 while (1) { # forever
   # get a set of readable handles (blocks until at least one handle is ready)
   my ($rh_set) = IO::Select->select($read_set, undef, undef, 0);
   # take all readable handles in turn
   foreach my $rh (@$rh_set) {
      # if it is the main socket then we have an incoming connection and
      # we should accept() it and then add the new socket to the $read_set
      if ($rh == $s) {
         my $ns = $rh->accept();
         $read_set->add($ns);
		 print "Connection accepted on port ".$ns->sockport()."\n";
		 createPlayer($ns);
		 print $ns "Welcome\n";
      }
      # otherwise it is an ordinary socket and we should read and process the request
      else {
         my $buf = <$rh>;
		 #my $status = sysread($rh,$buf,512);
		 #if($status>0){ $buf .= <$rh> ; }
         if($buf) { # we get normal input
             # ... process $buf ...
			 print "received:\n".$buf;
			 print $rh "Roger that\n";
			 #sendError($rh, "Roger that");
         }
         else { # the client has closed the socket
             # remove the socket from the $read_set and close it
			 print "Connection closed\n";
             $read_set->remove($rh);
             close($rh);
         }
      }
   }
 }
 
 close($s);
 
 #------------- GAME HANDLING THINGS ---#
 
 #createPlayer, accept a socket handle, create an array entry with associated hash and respond to player
 sub createPlayer {
	my $rh = shift;
	my $padd = $rh->peerhost();
	#print "peer connection. addr: ".$rh->peeraddr()."  host:".$rh->peerhost()." port:".$rh->peerport()."\n";
	$users{$padd} = { 'status'=>'new' };
	print $padd . " new player - ".$users{$padd}."\n";
 }
 
 #handleMSG - main handler. takes peer host and message and decides what to do with it. 
 sub handleMSG {
	my $addr, my $buf = shift; #input host address and buffered msg
	my %response = {'type'=>'','msg'=>''};
 }
 
 sub sendError {
	my $rh; my $errMSG;
	($rh, $errMSG) = shift;
	print "rh ".$rh."\n";
	print "errMSG ".$errMSG."\n";
	my %response = ('type'=>'error','msg'=>$errMSG);
	my $rstr = encode_json \%response;
	print $rstr."\n";
	#print $rh $rstr;
 }