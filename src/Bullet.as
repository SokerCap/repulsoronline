/* Bullet class - used for all projectiles
   Can follow straightforward accelleration or use plots on a math function (sin,cos waves) to determine path
*/
package
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.Tween;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.tweens.motion.*;
	import net.flashpunk.utils.Ease;
	
	public class Bullet extends Entity
	{
		[Embed(source = 'data/gfx/player/ship_t.png')]
		private const pewSheet:Class;
		
		public static var Sprites:Spritemap;
		public static var SpritesMap:BitmapData;
		private var pattern:Number = 0; 
		/* Pattern = math function used to determine path of projectile 
		   0 = straight flight
		   1 = sin wave
		   (more to come)
		*/
		public var target:Entity = null; //set this in order to have bullet home in on an entity
		public var polarity:Number = 0; // is bullet red/blue?
		private var dir:Number = 0;
		private var dX:Number = 0;
		private var dY:Number = 0;
		private var dirR:Number = 0; //direction in radians  = stats["dir"] * Math.PI / 180;
		public var twPath:Motion;
		private var wpn:Array;
		
		private var fuckinSMAP:Spritemap;
		
		public function Bullet(shooter:Entity, Weapon:Array, polarity:Number = 0, graphic:Graphic=null, mask:Mask=null)
		{
			//super(x, y, graphic, mask);
			wpn = Weapon;
			//graphic = sMap;
			fuckinSMAP = new Spritemap(Bullet.SpritesMap,10,10);
			fuckinSMAP.add("red",[7],1,true);
			fuckinSMAP.add("blue",[0],1,true);
			
			this.graphic = fuckinSMAP; graphic = this.graphic;
			if(wpn["polarity"] == "blue"){
				polarity = 1; fuckinSMAP.play("blue");
			}
			else {
				fuckinSMAP.play("red");
			}
			type = "bullet";
			//graphic = new Image(pewSheet,new Rectangle(0,0,10,10));
			
			//if(polarity == 0) fuckinSMAP.play("red");
			//else fuckinSMAP.play("blue");
			dir = Image(shooter.graphic).angle;
			dirR = dir * Math.PI / 180;
			//x = shooter.x + (Math.cos(dirR) * 20); 
			//y = shooter.y + (Math.sin(dirR) * -20);
			x = shooter.x + 25;
			y = shooter.y + 25;
			Image(this.graphic).angle = dir ;
			initPath(shooter); layer = 1;
			
			//trace("Bullet created at ("+dirR+") "+x+"("+shooter.x+"),"+y+"("+shooter.y+")");
			graphic = this.graphic;
			
		}
		
		//setup bullet path function based on shooter position and bullet pattern/type
		private function initPath(shooter:Entity):void
		{
			//linear path
			// 50 is bullet speed - alter based on powerups/bullet type
			dX = Math.sin(dirR) * 220 * -1  ;
			dY = Math.cos(dirR) * 220 *-1  ;
			x += dX / 150 * 22; y += dY / 150 * 22 ; //move the bullet to in front of the ship
			//create motion tween for bullet type
			if(wpn["path"] == "quad") // quadratic path
			{
				//twPath = new QuadMotion(kill,2);
				twPath = new QuadPath(kill,2);
				QuadPath(twPath).addPoint(x,y);
				var dA:Number;
					if(wpn["dA"]) dA = wpn["dA"];
					else dA = Math.random()*2-1;
				var dX1:Number = Math.sin(dirR+dA)*220*-1;
				var dY1:Number = Math.cos(dirR+dA)*220*-1;
				QuadPath(twPath).addPoint(x+dX1*0.5,y+dY1*0.5);
				QuadPath(twPath).addPoint(x+dX,y+dY);
				QuadPath(twPath).addPoint(x+dX*2,y+dY*2);
				QuadPath(twPath).setMotionSpeed(210);
				//QuadMotion(twPath).setMotionSpeed(x,y,1/15,1/15,x+dX*15,y+dY*15,300);
				//QuadMotion(twPath).setMotion(x,y,1,1,x+dX*5,y+dY*5,5);
				
				
				
			}
			else if(wpn["path"] == "linear" || 1){ //linear by default
				twPath = new LinearPath(kill,2);
				LinearPath(twPath).addPoint(x,y);
				LinearPath(twPath).addPoint(x+dX*100,y+dY*100); 
				LinearPath(twPath).setMotionSpeed(250);
				//LinearPath(twPath).setMotionSpeed(250,Ease.bounceInOut); //how to add an ease
				
			}
			else {
				twPath = new Motion(20,kill,2,Ease.bounceInOut);	
				twPath.x = x; twPath.y = y;
				//twPath.addPoint(x,y);
				//twPath.addPoint(x+dX*10,y+dY*10);
				
			}
			addTween(twPath,true);
			//trace("added linear tween with "+twPath.pointCount+" points - "+twPath.getPoint(1).x+","+twPath.getPoint(1).y);
		}
		
		override public function update():void
		{
			x = twPath.x ; y = twPath.y;
			//x += dX * FP.elapsed; y += dY * FP.elapsed; 
			if(x < 0 || x > 800 || y < 0 || y > 600) kill();
			else super.update();
			
		}
		public function kill():void
		{
			FP.world.remove(this);
		}
	}
}