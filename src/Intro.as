package
{
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	public class Intro extends World
	{
		[Embed(source = 'data/gfx/bg/titlescreen.png')]
		private const TITLE:Class;
		
		private var entTitle:Entity;
		private var entShippy:Shippy;
		private var isLoading:Boolean = true;
		private var titleSong:Sound;
		private var titleSongC:SoundChannel;
		
		public function Intro()
		{
			//super();
			entShippy = new Shippy(); entShippy.x = 512; entShippy.y = 400;
			add(entShippy);
			
			addDatas();
			//add(entTitle);
			
			
		
		}
		
		override public function update():void
		{
			if(isLoading) Image(entShippy.graphic).angle += 2;
			
			if(Input.check(Key.SPACE))
			{
				if(titleSongC) titleSongC.stop();
				FP.world = new PlayState;
			}
			
		}
		
		public function addDatas():void
		{
			entTitle = new Entity();
			entTitle.graphic = new Image(TITLE);
			
			titleSong = new Sound();
			titleSong.load(new URLRequest("data/sfx/titlesong.mp3"));
			titleSong.addEventListener(Event.COMPLETE,songComplete,false,0,true);
			trace("Datas added!");
		}
		
		private function songComplete(evt:Event):void
		{	
			isLoading = false;
			remove(entShippy);
			titleSongC = titleSong.play();
			add(entTitle);
		}
	
	}
}