/* LevelLoader.as
   Load level xml file, parse out data resources and load them dynamically from packaged zip
   Requires fzip library to load zipped level data - http://codeazur.com.br/lab/fzip/

*/

package
{
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	
	import net.flashpunk.graphics.Image;

	
	
	public class LevelLoader
	{
		// TEMP - embedded xml used for testing gameplay without dealing with dynamic loading
		//[Embed(source="data/levels.xml", mimeType="text/xml")]
		//protected const EmbeddedXML:Class;
		public var gameXML:XML ;
		// --
		
		public var isLoaded:Boolean = false;
		
		private var xmlLoader:URLLoader = new URLLoader();
		private var zipLoader:URLLoader = new URLLoader();
		private var xmlURL:String = new String();
		private var zipURL:String = new String();
		
		//arrays containing game assets
		public var Assets:Array = new Array(); //array containing actual assets keyed by dir/filename
		public var playerAssets:Array = new Array(); //array of keys pointing to entries in Assets for player
		public var enemyAssets:Array = new Array(); //array of keys pointing to entries in Assets for enemies
		public var AssetsR:Array = new Array(); //associative array pointing to assets
		
		private var zip:FZip;
		private var index:uint = 0;
		private var count:uint = 0;
		private var done:Boolean = false;
		
		public var curLVL:int = 0;
		public var maxLVL:int = 0;
		public var curWave:int = 0;
		public var maxWave:int = 0;
		
		public function LevelLoader()
		{
			//gameXML = XML(new EmbeddedXML());
			//trace("xml loaded: ");
			//trace(gameXML.*);
			loadGame("data/levels.xml");
		}
		
		public function loadGame(gameURL:String):void
		{
			xmlURL = gameURL;
			xmlLoader.load(new URLRequest(xmlURL));
			xmlLoader.addEventListener(Event.COMPLETE,onGameXMLLoad);
		}
		
		private function onGameXMLLoad(evt:Event):void
		{
			trace("XML at "+xmlURL+" Loaded");
			gameXML = new XML(evt.target.data);
			trace(gameXML.level[0].@text);
			if(gameXML.meta.pak){ loadData(gameXML.meta.pak.@src); }
			curLVL = 0;
			maxLVL = gameXML.level.length();
			trace('curlvl: '+curLVL+" maxlvl:"+maxLVL);
			
		}
		
		private function loadData(dataURL:String):void
		{
			trace("loading pak at "+dataURL);
			zip = new FZip();
			zip.addEventListener(Event.COMPLETE, onDataLoad);
			zip.load(new URLRequest(dataURL));
			
		}
		private function onDataLoad(evt:Event):void
		{
			trace("dataloaded - "+zip.getFileCount());
			var fCount:uint = zip.getFileCount();
			var loader:Loader = new Loader();
			var bmp:Bitmap; 
			for(var i:uint = 0; i < 300; i++) {
				if(i < fCount) {
					var file:FZipFile = zip.getFileAt(i);
					if(file.filename.indexOf(".png") != -1 || file.filename.indexOf(".jpg") != -1 )
					{
						//trace("Adding "+file.filename);
						
						Assets[index] = new Loader();
						Assets[index].loadBytes(file.content);
						//trace(file.filename+" - "+file.content);
						AssetsR[file.filename] = Assets[index];
						index++;
					}
					else if(file.filename.indexOf(".mp3") != -1)
					{
						//trace("MP3 NOT SUPPORTED - "+file.filename);
					}
					
				}
				else {
					isLoaded = true;
					break;
				}
			}
			trace("done loading "+Assets.length+" assets");
			trace(AssetsR["gfx/player/ship_splode3.png"]);
			
		}
	}
}