/* PlayState.as
   General state for all gameplay. Handles loading level progression internally
*/
package
{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.utils.ByteArray;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	public class PlayState extends World
	{
		public var WorldW:int = 1200;
		public var WorldH:int = 800;
		public const ScreenW:int = 800;
		public const ScreenH:int = 600;
		private var cameraOffset:int = 200;
		private var cameraSpeed:int = 6;
		public var socket:CustomSocket;
		
		public static var Loader:LevelLoader = new LevelLoader();
		
		private var isStarted:Boolean = false;
		private var entShippy:Shippy ;
		private var curBG:Entity = new Entity();
		
		
		private var eSprites:Array = new Array(); //store enemy spritesheet by enemy name
		
		public var lvlText:Entity = new Entity();
		public var lvlTexttxt:Text ;
		private var txtLevel:Entity = new Entity(300,10);
		private var txtShield:Entity = new Entity(430,10);
		private var imgShield:Entity = new Entity(500,10); //shield left gui image
		private var txtRepulsor:Entity = new Entity(700,10);
		private var imgRepulsor:Entity = new Entity(700,10); //repulse left gui image
		private var imgLife:Image ;//lives left graphic.
		private var txtWeapon:Entity = new Entity(20,560); //current weapon text
		private var txtLoc:Entity = new Entity(ScreenW-150,ScreenH-50);
		
		
		
		private var Bullets:Array = new Array();
		private var playStatus:String = "init";
		private var lvl:XML ;
		
		private var Weapons:Array = new Array("Red Pulse","Blue Pulse","Red Quad","Blue Quad","Red Wave","Blue Wave","Alt Wave","Alt Quad");
		private var curWPN:int = 0;
		
		public function PlayState()
		{
			//super();
			curBG.layer = 100;
			add(curBG);
			
			entShippy = new Shippy ; entShippy.x = 512; entShippy.y = 400;
			add(entShippy);
			
			lvlTexttxt = new Text("Loading Assets..");
			lvlTexttxt.visible = true; lvlTexttxt.color = 0xFFFFFF; lvlTexttxt.centerOrigin();
			lvlText.layer = 1; 
			lvlText.x = 300; lvlText.y = 300;
			lvlText.graphic = lvlTexttxt; 
			add(lvlText);
			
			socket = new CustomSocket("192.168.2.51",8080);
			
			
			
		}
		
		override public function update():void
		{
			//if(!isStarted) Image(entShippy.graphic).angle += 2;
			
			if(Loader.isLoaded && !isStarted){
				//entShippy.graphic = Image( Bitmap(Loader.AssetsR["gfx/enemies/enemy3_move.png"].content).bitmapData );
				
				if(Loader.AssetsR["gfx/enemies/e2.png"].content){
					if(loadSprites())
					{
						
						trace("Level loaded, changed ship graphic");
						isStarted = true;
					}
				}
				
			}
			else {
				if(isStarted && playStatus == "ready" && lvlText.visible){
					if(Input.pressed(Key.SPACE)){
						lvlText.visible = false; entShippy.isDisabled = false;
						playStatus = "playing";
					}
				}
				if(isStarted && playStatus == "playing" && entShippy.isDisabled == false ){
				//MAIN LOOP???!?!?!?
					Text(txtLoc.graphic).text = String(int(entShippy.x))+","+String(int(entShippy.y))+" ";
					if(Input.pressed(Key.SPACE)){ //FIRE???
						
						entShippy.pew();
						//add(pewpew);
					}
					if(Input.pressed(Key.Z)){ //Change weapon
						entShippy.switchWPN();
						txtWeapon.graphic = new Text("Weapon: "+entShippy.Weapons[entShippy.curWPN]);
					}
					if(classCount(Enemy) < 1){ //next wave
						entShippy.isDisabled = true; playStatus = "loading";
						startWave(Loader.curWave + 1);
					}
					
				}
			}
			followCamera();
			super.update();
			
		}
		
		//go through loaded raw images, convert to Image and load appropriate classes.
		private function loadSprites():Boolean
		{
			//trace(Loader.AssetsR["gfx/enemies/e2.png"].content);
			//Loader.AssetsR["gfx/enemies/e2.png"] = new Image(Loader.AssetsR["gfx/enemies/e2.png"].content.bitmapData);
			//trace(Loader.AssetsR["gfx/enemies/e2.png"]);
			//entShippy.graphic = new Image(Loader.AssetsR["gfx/enemies/enemy3_move.png"].content.bitmapData);
			Loader.AssetsR["gfx/player/shippy.png"] = Loader.AssetsR["gfx/player/shippy.png"].content.bitmapData;
			
			//load background
			Loader.AssetsR["gfx/bg/background.png"] = new Image(Loader.AssetsR["gfx/bg/background.png"].content.bitmapData);
			curBG.graphic = Loader.AssetsR["gfx/bg/background.png"];
			
			
			//load ship spritesheet
			var pSprites:Array = new Array();
			pSprites["move"] = Loader.AssetsR["gfx/player/shippy.png"]
			trace("pSpritesmove - "+pSprites["move"]);
			entShippy.loadSprites(pSprites);
			lvlText.graphic = new Text("Game On! (press space)"); 
			
			//load bullets spritesheet
			if(Loader.AssetsR["gfx/other/pew.png"]){
				Loader.AssetsR["gfx/other/pew.png"] = Loader.AssetsR["gfx/other/pew.png"].content.bitmapData;
				Bullet.SpritesMap = Loader.AssetsR["gfx/other/pew.png"];
				Loader.AssetsR["gfx/other/pew.png"] = new Spritemap(Loader.AssetsR["gfx/other/pew.png"],10,10 );
				Loader.AssetsR["gfx/other/pew.png"].add("red",[7],1,true);
				Loader.AssetsR["gfx/other/pew.png"].add("blue",[0],1,true);
				Loader.AssetsR["gfx/other/pew.png"].add("redsplode",[12,3,8,9],8,false);
				Loader.AssetsR["gfx/other/pew.png"].add("bluesplode",[6,11,12,9],8,false);
				//Bullet.Sprites = Loader.AssetsR["gfx/other/pew.png"]; */
			}
			
			//load enemy sprites
			if(Loader.AssetsR["gfx/enemies/e1.png"]){
				Enemy.sMaps["0"] = Loader.AssetsR["gfx/enemies/e1.png"].content.bitmapData;
				Loader.AssetsR["gfx/enemies/e1.png"] = Loader.AssetsR["gfx/enemies/e1.png"].content.bitmapData;
				Loader.AssetsR["gfx/enemies/e1.png"] = new Spritemap(Loader.AssetsR["gfx/enemies/e1.png"],41,41);
				Loader.AssetsR["gfx/enemies/e1.png"].add("move",[0,1,2,1],10,true);
				Loader.AssetsR["gfx/enemies/e1.png"].add("fire",[2,3,4,3],10,false);
				Loader.AssetsR["gfx/enemies/e1.png"].add("splode",[4,6,7,5,8],10,false);
				eSprites["0"] = Loader.AssetsR["gfx/enemies/e1.png"];
			}
			if(Loader.AssetsR["gfx/enemies/e2.png"]){
				Enemy.sMaps["1"] = Loader.AssetsR["gfx/enemies/e2.png"].content.bitmapData;
				Loader.AssetsR["gfx/enemies/e2.png"] = Loader.AssetsR["gfx/enemies/e2.png"].content.bitmapData;
				
				Loader.AssetsR["gfx/enemies/e2.png"] = new Spritemap(Loader.AssetsR["gfx/enemies/e2.png"],84,93);
				Loader.AssetsR["gfx/enemies/e2.png"].add("move",[0,1],10,true);
				Loader.AssetsR["gfx/enemies/e2.png"].add("fire",[2,4],10,false);
				Loader.AssetsR["gfx/enemies/e2.png"].add("splode",[4,5,6,3],10,false);
				eSprites["1"] = Loader.AssetsR["gfx/enemies/e2.png"];
			}
			
			loadGUI();
			startLevel(0);
			return true;
		}
		
		//load game level if another one/ start first wave
		private function startLevel(lvlNum:int):void
		{
			playStatus = "loading level";
			if(lvlNum >= Loader.maxLVL) return; //- TODO: game completion here
			Loader.curLVL += 1;
			lvl = Loader.gameXML.level[lvlNum] ;
			//bg
			if(Loader.AssetsR["gfx/bg/"+lvl.@background])
			{
				if(!(Loader.AssetsR["gfx/bg/"+lvl.@background] is Image))
				{
					Loader.AssetsR["gfx/bg/"+lvl.@background] = new Image(Loader.AssetsR["gfx/bg/"+lvl.@background].content.bitmapData);
				}
				curBG.graphic = Loader.AssetsR["gfx/bg/"+lvl.@background]; 
				//curBG.graphic.scrollX = 0; curBG.graphic.scrollY = 0; //scrolling background o no?
				//curBG.graphic = new Backdrop(curBG.graphic,true,false);
				
			}
			Loader.maxWave = lvl.Wave.length() ; 
			txtLevel.graphic = new Text("Level: "+(lvlNum+1));
			if(lvl.@text ) lvlText.graphic = new Text(lvl.@text);
			else lvlText.graphic = new Text("Level "+(lvlNum+1)+" loaded. Game On!");
			lvlText.x = 10;
			entShippy.x = 400; entShippy.y = 300; 
			Loader.curLVL = lvlNum;
			startWave(0);
			playStatus = "ready"; //waiting for player input to start
			lvlText.visible = true;
		}
		private function startWave(waveNum:int):void
		{
			trace("Starting lvl:"+Loader.curLVL+" wave:"+Loader.curWave);
			if(waveNum > 0) { 
				lvlText.graphic = new Text("Wave "+(waveNum+1)); lvlText.visible = true; 
				
			}
			if(waveNum > Loader.maxWave)
			{
				trace("Level complete!");
				startLevel(Loader.curLVL + 1); //finished waves
				return;
			}
			
			
			var curWave:XML = lvl.wave[waveNum];
			//add enemies
			var numE:int = 0;
			if(curWave.enemy) numE = curWave.enemy.length() ;
			trace("Wave "+waveNum+" loading "+numE+" enemies");
			//trace(curWave);
			for(var i:int=0; i<curWave.enemy.length(); i++)
			{
				var eXML:XML = curWave.enemy[i];
				var eType:String = "0"; 
				var eX:Number = Number(eXML.@positionx) ;
				var eY:Number = Number(eXML.@positiony);
				if(eXML.@type && eSprites[eXML.@type]) eType = eXML.@type ;
				var e:Enemy = new Enemy(eType, eX, eY, eSprites[eType], eXML);
				e.x = eX; e.y = eY; //for some reason constructor isnt setting x, y correctly
				e.target = entShippy;
				//trace("New enemy. Type:"+eXML.@type+" at "+e.x+","+e.y);
				add(e);
			}
			//trace(eSprites["1"]);
			//add(new Enemy("1",200,200,eSprites["1"]));
			Loader.curWave = waveNum;
			
			playStatus = "ready";
		}
		
		private function loadGUI():void
		{
			txtLevel.graphic = new Text("Level: 1");
			txtShield.graphic = new Text("Shield:                 Repulsor:");
			txtWeapon.graphic = new Text("Weapon: Red Pulse");
			add(txtLevel); add(txtShield); add(txtWeapon);
			imgRepulsor.graphic = Image.createRect(60,20,0x00FF00);
			imgShield.graphic = Image.createRect(60,20,0x00FF00);
			imgRepulsor.graphic.scrollX = 0; imgRepulsor.graphic.scrollY = 0;
			imgShield.graphic.scrollX = 0; imgShield.graphic.scrollY = 0;
			add(imgRepulsor); add(imgShield);
			txtLoc.graphic = new Text("x      ,    y");
			add(txtLoc);
			
			var testO:Object = {
				type: 'test'
			};
			trace(testO);
			socket.sendObject(testO);
		}
		
		/* followCamera():void
		   move the camera to center to the player (except on boundries) */
		public function followCamera():void
		{
			if(entShippy.x - FP.camera.x < cameraOffset){
				if(FP.camera.x > 0) FP.camera.x -= cameraSpeed;
			}
			else if ((FP.camera.x + FP.width) - (entShippy.x + entShippy.width) < cameraOffset){
				if(FP.camera.x+FP.width< WorldW) FP.camera.x += cameraSpeed;
			}
			if(entShippy.y - FP.camera.y < cameraOffset){
				if(FP.camera.y > 0) FP.camera.y -= cameraSpeed;
			}
			else if((FP.camera.y + FP.height)-(entShippy.y + entShippy.height) < cameraOffset) {
				if (FP.camera.y + FP.height < WorldH) FP.camera.y += cameraSpeed;
			} 
			
		}
		
	}
}