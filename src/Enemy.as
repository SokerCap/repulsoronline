package
{
	import flash.display.BitmapData;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	
	public class Enemy extends Entity
	{
		private const eTypes:Array = new Array("Redpew","Bluepew","fastred"); //list of enemy type names? wejnsekjns
		public var eType:String;
		public var target:Shippy;
		private var timers:Array = new Array();
		private var aistats:Array = new Array(); //variables for deciding next action
		private var aistatus:String = "chase";
		private var dir:Number ; //direction in angle
		private var dirP:Number ; //direction in radians
		public var eXML:XML;
		private var sMap:Spritemap;
		public var dX:Number = 0; public var dY:Number = 0;
		public var isDying:Boolean = false;
		
		public static var sMaps:Array = new Array(); //hold array of bitmapdata used to create spritemaps for new enemies
		
		//public var stats:Array = new Array("hp"=>0,"weapon"=>0,"polarity"=>0);
		
		public function Enemy(etype:String, x:Number, y:Number, gfx:Spritemap=null, eXML:XML=null, mask:Mask=null)
		{
			timers["firewait"] = 10; //seconds before can fire again
			eType = etype;
			type = "enemy";
			layer = 50;
			if(gfx) graphic = gfx;
			initType();
			if(graphic is Spritemap) gfx.play("move");
			dir = 0 ; dirP = 0;
			if(eXML && eXML.@initialdir){
				dir = Number(eXML.@initialdir);
				Image(graphic).angle = dir;
				//trace("set enemy graphic to dir "+dir);
			}
			
			//super(x, y, graphic, mask);
		}
		
		/* initialize graphics and starting attributes based on enemy type */
		private function initType():void
		{
			switch (eType)
			{
				case "0":
					sMap = new Spritemap(sMaps["0"],41,41);
					sMap.add("move",[0,1,2,1],10,true);
					sMap.add("fire",[2,3,4,3],10,false);
					sMap.add("splode",[4,6,7,5,8],10,false);
					graphic = sMap; sMap.play("move");
					setHitbox(35,35);
					break;
				case "1":
					sMap = new Spritemap(sMaps["1"],84,93);
					sMap.add("move",[0,1],10,true);
					sMap.add("fire",[2,4],10,false);
					sMap.add("splode",[4,5,6,3],10,false);
					graphic = sMap; sMap.play("move");
					setHitbox(84,93);
					break;
				default:
					trace("error initializing enemy - no type defined");
					break;
			}
		}
		
		//find direction to point at enemy
		private function enemyAngle():Number
		{
			
			if(!target) return 0;
			if(target && target.isDisabled) return 0; 
			var dirrad:Number = Math.atan(Math.abs( (target.x-x) /(target.y-y) ) );
			var dird:Number = dirrad * 180 / Math.PI ;
			//trace("dirrad: "+dirrad+"  dird:"+dird+ "("+target.x+"-"+x+")/("+target.y+"-"+y+")");
			if(target.x < x && target.y > y) dird = 180 - dird;
			else if(target.x > x && target.y > y ) dird = 180 + dird;
			else if(target.x > x && target.y < y ) dird = 360 - dird ;
			//trace("dir: "+dir+"  dird: "+dird);
			
			if(Math.ceil(dird) != Math.ceil(dir) ){ //for testing always point to current direction of enemy
				//if(Math.abs(dird-Image(graphic).angle) > 10) 
				Image(graphic).angle = Math.ceil(dird);
				
				dir = Math.ceil(dird);
				//trace('adjusting direction for '+x+","+y+" to "+target.x+","+target.y+" - "+dir);
			}
			return Math.ceil(dird);	
		}
		
		override public function update():void
		{
			if(timers["firewait"] > 0) timers["firewait"] -= 1;
			if(isDying){
				if(sMap.complete) FP.world.remove(this);
				return;
			}
			if(target && target.isDisabled == false){
				//collision detection
				var ouch:Bullet = collide("bullet",x,y) as Bullet;
				if(ouch){  FP.world.remove(ouch); kill(); }
				
				var newd:Number = enemyAngle();
				dirP = dir * Math.PI / 180;
				dX += (Math.sin(dirP) * 60) * -1 * FP.elapsed;
				dY += (Math.cos(dirP) * 60) * -1 * FP.elapsed;
				if(dX < -120) dX = -120;
				if(dX > 120) dX = 120;
				if(dY < -120) dY = -120;
				if(dY > 120) dY = 120;
				x += dX*FP.elapsed; y += dY*FP.elapsed;
				if(x < 10) x = 10;
				if(x > 760) x = 760;
				if(y < 10) y = 10;
				if(y > 560) y = 560;
			}
		
		}
		public function kill():void
		{
			isDying = true;
			sMap.play("splode");
		}
	}
}