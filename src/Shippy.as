/* Shippy.as
   Player ship entity
*/

package
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	
	public class Shippy extends Entity
	{
		[Embed(source = 'data/gfx/player/ship_t.png')]
		private const PLAYER:Class;
		private var smaps:Array = new Array(); //hold spritemaps
		
		public var stats:Array = new Array();
		
		private var timers:Array = new Array(); //array of timers for various ship events
		
		public var isDisabled:Boolean = true; //is it currently player moveable
		
		public var Weapons:Array = new Array("Red Pulse","Blue Pulse","Red Quad","Blue Quad","Red Wave","Blue Wave","Alt Wave","Alt Quad");
		public var curWPN:int = 0;
		
		public function Shippy()
		{
			//super(x, y, graphic, mask);
			//graphic = new Image(PLAYER);
			x = 200; y = 150;
			stats["dX"] = 0; //current speed on X
			stats["dY"] = 0; //current speed on Y
			stats["aX"] = 0; //current accel X
			stats["aY"] = 0; //accel y
			stats["maxA"] = 50; //max acceleration;
			stats["thrust"] = 40; //thruster power - how much to accelerate by
			stats["maxD"] = 195; //max speed
			stats["dir"] = 0; //direction. base sprite is 0=North 90=East etc
			timers["thrusting"] = 0; //ooo yaaa
			stats["shield"] = 100;
			stats["repulsor"] = 100;
			
			//controls
			Input.define("Shoot", Key.SPACE);
			Input.define("Forward", Key.UP);
			Input.define("Left", Key.LEFT);
			Input.define("Right", Key.RIGHT);
			Input.define("Reverse", Key.DOWN);
			Input.define("Polarity", Key.SHIFT);
			Input.define("Repulse", Key.CONTROL);
			Input.define("Weapon", Key.Z);
			
			graphic = new Image(PLAYER);
			layer = 30;
			
			
	
		}
		
		//player is firing, initiate proper stuffs
		public function pew():void
		{
			var wpn:Array = new Array();
			wpn["path"] = "linear";
			wpn["count"] = 0;
			wpn["name"] = Weapons[curWPN];
			wpn["polarity"] = "blue";
			if(curWPN == 2 || curWPN == 3) wpn["path"] = "quad" ;
			else if(curWPN == 4 || curWPN == 5) wpn["path"] = "sin";
			if(curWPN == 0 || curWPN == 2 || curWPN == 4) wpn["polarity"] = "red";
			if(wpn["path"] == "quad")
			{
				wpn["dA"] = 1;
				var pewpew:Bullet = new Bullet(this,wpn,0);
				FP.world.add(pewpew);
				wpn["dA"] = -1;
				pewpew = new Bullet(this,wpn,0);
				FP.world.add(pewpew);
			}
			else {
				var pewpew:Bullet = new Bullet(this,wpn,0);
				FP.world.add(pewpew);
			}
			
		}
		
		public function switchWPN():void //switch to next available weapon
		{
			curWPN += 1;
			if(curWPN >= Weapons.length) curWPN = 0;
		}
		
		//once spritesheets are loaded, pass them here
		public function loadSprites(imgs:Array):void
		{
			if(!imgs["move"]){ trace("MISSING MOVE PLAYER ANIMATIONS"); return; }
			smaps["move"] = new Spritemap(imgs["move"],48,49);
			smaps["move"].originX = 24; smaps["move"].originY = 24;
			smaps["move"].add("stand",[0,0,5,6,5,0],10,true);
			smaps["move"].add("move",[2,7,7,7,2],20,true);
			smaps["move"].add("splode",[0,0,8,9],10,false);
			graphic = smaps["move"];
			smaps["move"].play("stand",true);
			//isDisabled = false;
			trace('started stand animation');
		}
	
		public function rotate(ang:int):void
		{
			Image(graphic).angle = ang;
		}
		// turn left(-amt) or right(+amt) with amt as degree EDIT: left=+amt CAUSE FLASHPUNK SUCKS DICKS
		public function turn(amt:Number):void
		{
			var newAng:Number = Image(graphic).angle + amt;
			if(newAng > 360) newAng = newAng - 360;
			if(newAng < 0) newAng = 360 + newAng ;
			//trace("new angle: "+newAng);
			Image(graphic).angle = newAng;
			stats["dir"] = newAng;
		}
		
		override public function update():void
		{
		
			if(smaps && smaps["move"])
			{
			
				if(!isDisabled){
					var newX:Number = x; var newY:Number = y;
					var dirR:Number = stats["dir"] * Math.PI / 180;
					stats["aY"] = 0; stats["aX"] = 0;
					if(Input.check("Forward"))
					{
						
						stats["aY"] += Math.cos(dirR) * stats["thrust"] * -1 ;
						if(stats["aY"] > stats["maxA"]) stats["aY"] = stats["maxA"] ;
						stats["aX"] += Math.sin(dirR) * stats["thrust"]*-1 ; //why is x reversed??
						if(stats["aX"] > stats["maxA"]) stats["aX"] = stats["maxA"] ;
						//trace("dir:"+dirR+" thrustX:"+stats["aX"]+" thrustY:"+stats["aY"]);
						timers["thrusting"] = 10;
					}
					if(Input.check("Reverse"))
					{   /*
						stats["aY"] += Math.cos(dirR) * stats["thrust"] ;
						if(stats["aY"] > stats["maxA"]) stats["aY"] = stats["maxA"] ;
						if(stats["aY"] < stats["maxA"]*-1) stats["aY"] = stats["maxA"]*-1 ;
						stats["aX"] += Math.sin(dirR) * stats["thrust"]  ;
						if(stats["aX"] > stats["maxA"]) stats["aX"] = stats["maxA"] ;
						if(stats["aX"] < stats["maxA"]*-1 ) stats["aX"] = stats["maxA"]*-1 ;
						*/
						
						stats["dY"] = stats["dY"] * 0.9;
						stats["dX"] = stats["dX"] * 0.9;
						timers["thrusting"] = 10;
					}
					if(Input.check("Left"))
					{
						turn(+5);
					}
					if(Input.check("Right"))
					{
						turn(-5);
					}
					stats["dX"] += stats["aX"] ; stats["dY"] += stats["aY"]; //increase speed by accel
					if(stats["dX"] > stats["maxD"]) stats["dX"] = stats["maxD"] ; //cap speed to max
					if(stats["dY"] > stats["maxD"]) stats["dY"] = stats["maxD"] ;
					if(stats["dX"] < stats["maxD"]*-1) stats["dX"] = stats["maxD"]*-1 ;
					if(stats["dY"] < stats["maxD"]*-1) stats["dY"] = stats["maxD"]*-1 ;
					if(Math.abs(stats["dX"]) <= stats["thrust"] /2 ) stats["dX"] = 0; //bring to stop if slow enough
					if(Math.abs(stats["dY"]) <= stats["thrust"] /2 ) stats["dY"] = 0; //bring to stop if slow enough
					newX = x + ( stats["dX"]*FP.elapsed); newY = y + (stats["dY"]*FP.elapsed);
					
					
					if(timers["thrusting"] < 1){ smaps["move"].play("stand"); }
					else{ smaps["move"].play("move"); timers["thrusting"] -= 1; }
					
					/*if(newY < 660 && newY > 40) this.y = newY;
					if(newX > 40 && newX < 760) this.x = newX; */
					if(newY != this.y || newX != this.x){
						if(newY < PlayState(FP.world).WorldH - this.height && newY > 40) this.y = newY;
						if(newX > 40 && newX < PlayState(FP.world).WorldW - this.width) this.x = newX; 
						//PlayState(FP.world).followCamera();
					}
					
				}
				
			}
			
		}
	}
}