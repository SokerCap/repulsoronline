package
{
	import com.adobe.serialization.json.*;
	
	import flash.errors.*;
	import flash.events.*;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	
	public class CustomSocket extends Socket
	{
		private var response:String;
		private var responseO:Object;
		private var outgoingBA:ByteArray;
		private var isConnected:Boolean = false;
		
		public function CustomSocket(host:String=null, port:int=0)
		{
			super(host, port);
			configureListeners();
		}
		
		private function configureListeners():void {
			addEventListener(Event.CLOSE, closeHandler);
			addEventListener(Event.CONNECT, connectHandler);
			addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
		}
		
		private function writeln(str:String):void {
			str += "\n";
			try {
				writeUTFBytes(str);
			}
			catch(e:IOError) {
				trace(e);
			}
		}
		
		private function sendRequest():void {
			trace("sendRequest");
			response = "";
			writeln("GET /");
			flush();
		}
		
		private function sendStr(str:String):void{
			response = "";
			writeln(str);
			flush();
		}
		
		public function sendObject(o:Object):void {
			if(!isConnected) return; 
			sendStr(JSON.encode(o));
			//trace("send: "+ JSON.encode(o));
		}
		
	
		
		private function readResponse():void {
			var str:String = readUTFBytes(bytesAvailable);
			response += str;
			responseO = JSON.decode(str);
			
			trace("Read:" + responseO.data.name);
		}
		
		private function closeHandler(event:Event):void {
			trace("closeHandler: " + event);
			isConnected = false;
			trace(response.toString());
		}
		
		private function connectHandler(event:Event):void {
			trace("connectHandler: " + event);
			isConnected = true;
			sendRequest();
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
			trace("ioErrorHandler: " + event);
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void {
			trace("securityErrorHandler: " + event);
		}
		
		private function socketDataHandler(event:ProgressEvent):void {
			trace("socketDataHandler: " + event);
			readResponse();
			
		}	
	}
}